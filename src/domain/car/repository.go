package car

// Repository ...
type Repository interface {
	Find() Cars
	Save(c Car)
}
