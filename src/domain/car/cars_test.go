package car

import "testing"

func TestCountWhenEmptyCollection(t *testing.T) {
	c := NewCars()

	if c.Count() != 0 {
		t.Error("Collection must have zero elements")
	}
}
