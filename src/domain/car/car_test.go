package car

import "testing"

func TestPriceCannotBeZero(t *testing.T) {
	_, err := New("abc", "Ford", 0)

	if err == nil {
		t.Errorf("Car must be invalid")
	}
}
