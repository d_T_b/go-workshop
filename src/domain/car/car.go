package car

import "errors"

// Car ...
type Car struct {
	identity string
	name     string
	price    int32
}

// New ...
func New(i string, n string, p int32) (Car, error) {
	if p <= 0 {
		return Car{}, errors.New("Price cannot be null")
	}

	if p > 1000000 {
		return Car{}, errors.New("Price too expensive")
	}

	return Car{identity: i, name: n, price: p}, nil
}

// Identity ...
func (c Car) Identity() string {
	return c.identity
}

// Name ...
func (c Car) Name() string {
	return c.name
}

// Price ...
func (c Car) Price() int32 {
	return c.price
}
