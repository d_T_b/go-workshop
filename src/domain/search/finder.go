package search

import (
	"fmt"
	"time"
)

// Finder ...
type Finder struct {
}

// New ...
func New() Finder {
	return Finder{}
}

// Find ...
func (f Finder) Find(origin string, destination string) {
	fmt.Println("Start process for", origin, destination)

	data := make(chan int)

	go processor(data)

	select {
	case value := <-data:
		fmt.Println(value)
	case <-time.After(time.Second * 5):
		fmt.Println("timeout")
		close(data)
	}
}

// processor ...
func processor(data chan int) {
	time.Sleep(time.Second * 3)

	fmt.Println("Response done")

	select {
	case <-data:
		fmt.Println("process cancelled")
		return
	default:
		data <- 10
		close(data)
	}
}
