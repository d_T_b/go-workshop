package controller

import (
	"net/http"

	"bitbucket.org/xavividal/go-workshop/src/domain/search"
	"github.com/labstack/echo"
	"github.com/satori/uuid"
)

// SearchCreate ...
func SearchCreate(c echo.Context) error {
	f := search.New()
	go f.Find("BCN", "MAD")

	token := uuid.NewV4()

	return c.String(http.StatusOK, token.String())
}
