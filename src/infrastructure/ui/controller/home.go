package controller

import (
	"net/http"

	"github.com/labstack/echo"
)

// HomeMain ...
func HomeMain(c echo.Context) error {
	return c.String(http.StatusOK, "Hello World")
}

// HomeData ...
func HomeData(c echo.Context) error {
	type data struct {
		First  string `json:"a"`
		Second string `json:"b"`
	}

	a := data{First: "pete", Second: "sampras"}

	return c.JSON(http.StatusOK, a)
}
